# Behametrics - Machine learning 

Behametrics app allows developing application that can recognize user using only data from accelerometer, gyroscope and touch events from screen without interferencing of user. This part contains machine learning.

#### How to use our project (library)
This project is only one part of bigger project, which can by used by interaction with main Model class in ```Model.py``` file.

Steps:
1. Model(path_to_dataset, uuid_of_user)
2. Model.train()
3. Model.predict()

Dataset contrains .csv file with logged data from Logger application: https://gitlab.com/bytecrowd/behametrics

## Dependencies
- Python 3
- numpy
- pandas
- math

## Data preprocessing
### Overview
Code in this section is used for data manipulation as loading, saving, splitting and computing features.

### Code

Code in ```data_preprocessing.py``` and ```create_negative_samples.py``` is used for simple data manipulation and contains:
 - Spliting data by sensor or timestamp
 - Saving and loading data
 - Creating negative samples for learning

Code in ```features.py``` is used for computation of features and contains computation of:
 - touch velocity
 - touch acceleration
 - touch trajectory
 - touch distance
 - touch ratio
 - magnitude, mean, max, min, std, energy, range, median and iqrange

## Machine learning
### Overview
Code in this section is used for train and prediction of models.

### Code
Main class of machine learning is in ```Model.py```, which contains code for training and testing of model and for final prediction.
