#!/usr/bin/python3

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.externals import joblib
import numpy as np
import os
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

from data_preprocessing import split_by_timestamp, get_negative_samples
from features import get_features_for_all_sensors
from data_preprocessing import save_dataframe


def _save_frames(path, frames):
    for key, value in frames.items():
        print("saving " + str(key) + " features")
        save_dataframe(value, key, os.path.join(path, "segments/"))


class Model(object):

    def __init__(self, path_to_raw_data, path_to_models, uuid, sensors):
        self.uuid = uuid
        self.path_to_raw_data = os.path.join(path_to_raw_data, uuid)  # "/var/www/html/logger/datasets/"
        self.path_to_model = os.path.join(path_to_models, uuid)
        self.sensors = sensors
        self.sample_user = "/var/tmp/SAMPLE_USER"
        self.train_folder = "/var/tmp/train_data"
        self.models = {}

        if os.path.exists(self.path_to_model):
            for sensor in sensors:
                path_to_sensor = os.path.join(self.path_to_model, str(sensor) + ".joblib.pkl")
                if not os.path.exists(path_to_sensor):
                    continue
                self.models[sensor] = joblib.load(path_to_sensor)
        else:
            os.makedirs(self.path_to_model)
            for sensor in sensors:
                if sensor == 'gyro':
                    self.models[sensor] = KNeighborsClassifier(n_neighbors=24, weights='distance')
                    continue

                RFS = RandomForestClassifier(criterion='gini', max_features='auto', min_samples_leaf=1,
                                             n_estimators=200)
                self.models[sensor] = AdaBoostClassifier(base_estimator=RFS)

    def train(self):
        features = self._get_features(self.path_to_raw_data, self.sensors, train=True)
        print("training")

        keys_to_drop=[]
        for key in self.models.keys():
            print(key)
            if not key in features:
                #self.models.pop(key, None)
                keys_to_drop.append(key)
                continue
            value_for_fitting = features[key]
            y = value_for_fitting['class']
            value_for_fitting.drop('class', axis=1, inplace=True)
            self.models[key].fit(value_for_fitting, y)
            joblib.dump(self.models[key], os.path.join(self.path_to_model, str(key) + ".joblib.pkl"), compress=9)

        for key in keys_to_drop:
            self.models.pop(key, None)

        print("training done")
        os.system("rm -rf " + self.path_to_raw_data + "/*")
        # self.train_features = features

    def predict(self):
        # auth = False
        features = self._get_features(self.path_to_raw_data, self.sensors, train=False)

        if not features:
            print("empty folder with raw data for user: " + str(self.uuid))
            return

        predictions = {}
        for key, model in self.models.items():
            print(key)
            if not key in features:
                continue
            predictions[key] = model.predict(features[key])
            # TODO vyhodnocovanie asi inak premysliet
            print(np.count_nonzero(predictions[key] == 1) / predictions[key].__len__())
            if np.count_nonzero(predictions[key] == 1) / predictions[key].__len__() < 0.75:
                return False

            # print(auth)

        return True

    def _get_features(self, path, sensors=['move', 'acc', 'gyro'], train=False):
        # TODO noise filtering na surovych datach

        print("spliting: " + str(path))
        segments = split_by_timestamp(path, sensors)
        print("counting features")
        frames = get_features_for_all_sensors(segments)
        # TODO noise filtering na vypocitanych features
        new_path = os.path.join(self.train_folder)
        if train:
            print("moving dataset")
            if not os.path.exists(new_path):
                os.makedirs(new_path)
            os.system("rsync -a --remove-source-files " + path + " " + new_path)

            negative_samples = get_negative_samples(path=self.sample_user,
                                                    number_of_samples=0, sensors=sensors)
            for key in frames.keys():
                frames[key]['class'] = 1
                frames[key] = pd.concat([frames[key], negative_samples[key]], axis=0)
        else:
            os.system("rm -rf " + path + "/*")

        return frames

    def get_uuid(self):
        return self.uuid

    def set_sample_user(self, sample_user):
        self.sample_user = sample_user

    def set_train_folder(self, train_folder):
        self.train_folder = train_folder

# model = Model('C:\\behametrics_datasets', "C:\\behametrics_test_model", "test", ['move', 'acc', 'gyro'])
# model.set_sample_user("")
# # model = Model("/home/tommy/Projects/design-sprint-2018-01-13/datasets",
# #    "/home/tommy/Projects/design-sprint-2018-01-13/behametrics_test_model",
# #    "test", ['move', 'acc', 'gyro'])
# model.set_sample_user('C:\\behametrics_datasets\\SAMPLE_USER')
# model.train()
# print(model.predict())
