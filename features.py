import numpy as np
import pandas as pd
import math


def add_magnitude(segment):
    if 'z' in segment.columns.values:
        segment.loc[:, 'm'] = pd.Series(
            (segment['x'].astype(float) ** 2) + (segment['y'].astype(float) ** 2) + (segment['z'].astype(float) ** 2),
            index=segment.index)
    else:
        segment.loc[:, 'm'] = pd.Series((segment['x'].astype(float) ** 2) + (segment['y'].astype(float) ** 2),
                                        index=segment.index)

    segment['m'] = np.sqrt(segment['m'])


# simple features

def mean(column):
    return np.mean(column)


def max_value(column):
    return np.max(column)


def min_value(column):
    return np.min(column)


def std(column):
    return np.std(column)


def energy(column):
    return np.sum(np.power(column, 2))


def range_value(column):
    return max_value(column) - min_value(column)


def median(column):
    return column.median()


def iqrange(column):
    return column.quantile(0.75) - column.quantile(0.25)


# touch features returning array

def touch_velocity_for_axis(segment, column):
    last_time = segment['Timestamp'][0]
    velocity_array = []
    for time, coord in zip(segment['Timestamp'], segment[column]):
        delta_time = time - last_time
        last_time = time
        if delta_time == 0:
            velocity_array.append(0)
        else:
            displacement = math.fabs(last_coord - coord)
            velocity_array.append(displacement / delta_time)
        last_coord = coord
    return pd.DataFrame(data=velocity_array, columns=["touch_velocity_" + str(column)])


def touch_velocity(segment):
    last_time = segment['Timestamp'][0]
    velocity_array = []
    for time, coord_x, coord_y in zip(segment['Timestamp'], segment['x'], segment['y']):
        delta_time = time - last_time
        last_time = time
        if delta_time == 0:
            velocity_array.append(0)
        else:
            displacement = np.linalg.norm([last_x - coord_x, last_y - coord_y])
            velocity_array.append(displacement / delta_time)
        last_x = coord_x
        last_y = coord_y
    return pd.DataFrame(data=velocity_array, columns=["touch_velocity"])


def touch_acceleration_for_axis(segment, column, velocity_array):
    last_time = segment['Timestamp'][0]
    last_velocity = velocity_array[0]
    accel_array = []
    for time, coord, velocity in zip(segment['Timestamp'], segment[column], velocity_array):
        delta_time = time - last_time
        delta_velocity = velocity - last_velocity
        last_time = time
        last_velocity = velocity
        if delta_time == 0:
            accel_array.append(0)
        else:
            accel_array.append(delta_velocity / delta_time)
    return pd.DataFrame(data=accel_array, columns=["touch_acceleration_" + str(column)])


def touch_acceleration(segment, velocity_array):
    last_time = segment['Timestamp'][0]
    last_velocity = velocity_array[0]
    accel_array = []
    for time, coord_x, coord_y, velocity in zip(segment['Timestamp'], segment['x'], segment['y'], velocity_array):
        delta_time = time - last_time
        delta_velocity = velocity - last_velocity
        last_time = time
        last_velocity = velocity
        if delta_time == 0:
            accel_array.append(0)
        else:
            accel_array.append(delta_velocity / delta_time)
    return pd.DataFrame(data=accel_array, columns=['touch_acceleration'])


# touch features returning one number

def touch_trajectory_length_for_axis(column):
    return np.sum(np.absolute(np.diff(column.iloc[:].values)))


def touch_trajectory_length(segment):
    return np.sum(np.linalg.norm([np.diff(segment['x'].iloc[:]), np.diff(segment['y'].iloc[:])]))


def touch_distance_end_to_end_for_axis(column):
    return math.fabs(np.diff(column.iloc[[-1, 0]]))


def touch_distance_end_to_end(segment):
    return np.linalg.norm([np.diff(segment['x'].iloc[[-1, 0]]), np.diff(segment['y'].iloc[[-1, 0]])])


def touch_ratio_for_axis(column):
    distance = touch_distance_end_to_end_for_axis(column)
    if distance == 0:
        return 0
    return touch_trajectory_length_for_axis(column) / distance


def touch_ratio(segment):
    distance = touch_distance_end_to_end(segment)
    if distance == 0:
        return 0
    return touch_trajectory_length(segment) / distance


# combined methods for getting features

def get_simple_features(column, column_name):
    features = np.matrix(np.zeros([8, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (std(column))
    features[4] = (energy(column))
    features[5] = (range_value(column))
    features[6] = (median(column))
    features[7] = (iqrange(column))

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'std_' + column_name,
        'energy_' + column_name,
        'range_value_' + column_name,
        'median_' + column_name,
        'iqrange_' + column_name,
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)

	
def get_simple_features_TOUCH_X(column, column_name):
    features = np.matrix(np.zeros([6, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (energy(column))
    features[4] = (range_value(column))
    features[5] = (median(column))
    
    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'energy_' + column_name,
        'range_value_' + column_name,
        'median_' + column_name, 
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)
	
def get_simple_features_TOUCH_Y(column, column_name):
    features = np.matrix(np.zeros([5, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (energy(column))   
    features[4] = (median(column))
   

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'energy_' + column_name,       
        'median_' + column_name,
       
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)
		
def get_simple_features_TOUCH_M(column, column_name):
    features = np.matrix(np.zeros([5, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (energy(column))
    features[4] = (median(column))
  

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'energy_' + column_name,       
        'median_' + column_name,
        
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)	
	
	
	
	
	
	
def get_simple_features_GYRO_X(column, column_name):
    features = np.matrix(np.zeros([1, 1]))

    features[0] = (mean(column))
    

    headers = [
        'mean_' + column_name,
        
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)

def get_simple_features_GYRO_Y(column, column_name):
    features = np.matrix(np.zeros([8, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (std(column))
    features[4] = (energy(column))
    features[5] = (range_value(column))
    features[6] = (median(column))
    features[7] = (iqrange(column))

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'std_' + column_name,
        'energy_' + column_name,
        'range_value_' + column_name,
        'median_' + column_name,
        'iqrange_' + column_name,
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)

def get_simple_features_GYRO_Z(column, column_name):
    features = np.matrix(np.zeros([4, 1]))

   
    features[0] = (min_value(column))
    features[1] = (range_value(column))
    features[2] = (median(column))
    features[3] = (iqrange(column))

    headers = [
        'min_' + column_name,
        'range_value_' + column_name,
        'median_' + column_name,
        'iqrange_' + column_name,
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)

def get_simple_features_GYRO_M(column, column_name):
    features = np.matrix(np.zeros([5, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (energy(column))
    features[4] = (median(column))
  
    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'energy_' + column_name,
        'median_' + column_name,
       
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)

	
	
	
def get_simple_features_ACC_X(column, column_name):
    features = np.matrix(np.zeros([6, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (std(column))
    features[4] = (range_value(column))
    features[5] = (iqrange(column))

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'std_' + column_name,
        'range_value_' + column_name,
        'iqrange_' + column_name,
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)
	
def get_simple_features_ACC_Y(column, column_name):
    features = np.matrix(np.zeros([7, 1]))

    features[0] = (max_value(column))
    features[1] = (min_value(column))
    features[2] = (mean(column))
    features[3] = (std(column))
    features[4] = (energy(column))
    features[5] = (range_value(column))
    features[6] = (median(column))
    

    headers = [
        'max_' + column_name,
        'min_' + column_name,
        'mean_' + column_name,
        'std_' + column_name,
        'energy_' + column_name,
        'range_value_' + column_name,
        'median_' + column_name,
        
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)
	
		
		
def get_simple_features_ACC_Z(column, column_name):
    features = np.matrix(np.zeros([2, 1]))

   
    features[0] = (std(column))
    features[1] = (iqrange(column))

    headers = [
        'std_' + column_name,
        'iqrange_' + column_name,
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)
	
def get_simple_features_ACC_M(column, column_name):
    features = np.matrix(np.zeros([1, 1]))

    features[0] = (energy(column))
    

    headers = [
        'energy_' + column_name,
        
    ]

    return pd.DataFrame(data=features.transpose(), columns=headers)


	
def get_touch_features_filtered(segment):
	features = np.matrix(np.zeros([8, 1]))
	
	velocity=touch_velocity(segment)
	features[0]=energy(velocity)
	features[1]=touch_trajectory_length(segment)
	velocity_y=touch_velocity_for_axis(segment,'y')
	features[2]=max(velocity_y['touch_velocity_y'])
	features[3]=touch_ratio(segment)
	velocity_m=touch_velocity_for_axis(segment,'m')
	features[4]=energy(velocity_m)
	features[5]=touch_trajectory_length_for_axis(segment['x'])
	features[6]=energy(velocity_y)
	features[7]=touch_trajectory_length_for_axis(segment['m'])
	
	headers=[
		'energy_velocity',
		'trajectory_length',
		'max_velocity_y',
		'touch_ratio',
		'energy_velocity_m',
		'trajectory_length_x',
		'energy_velocity_y',
		'trajectory_length_m'
	]
	return pd.DataFrame(data=features.transpose(), columns=headers)	
	
	
	
	
	

def get_touch_features_for_axis(segment, column_name):
    features = np.matrix(np.zeros([3, 1]))

    features[0] = touch_trajectory_length_for_axis(segment[column_name])
    features[1] = touch_distance_end_to_end_for_axis(segment[column_name])
    features[2] = touch_ratio_for_axis(segment[column_name])

    headers = [
        'trajectory_length_' + column_name,
        'touch_distance_end_to_end_' + column_name,
        'touch_ratio_' + column_name,
    ]

    velocity = touch_velocity_for_axis(segment, column_name)
    features_dataframe = [pd.DataFrame(data=features.transpose(), columns=headers),
                          get_simple_features(velocity['touch_velocity_' + column_name], "velocity_" + column_name),
                          get_simple_features(
                              touch_acceleration_for_axis(segment, column_name,
                                                          velocity['touch_velocity_' + column_name])[
                                  'touch_acceleration_' + column_name],
                              "acceleration_" + column_name)]

    return pd.concat(features_dataframe, axis=1)


def get_touch_features(segment):
    features = np.matrix(np.zeros([3, 1]))

    features[0] = touch_trajectory_length(segment)
    features[1] = touch_distance_end_to_end(segment)
    features[2] = touch_ratio(segment)

    headers = [
        'trajectory_length',
        'touch_distance_end_to_end',
        'touch_ratio',
    ]

    velocity = touch_velocity(segment)
    features_dataframe = [pd.DataFrame(data=features.transpose(), columns=headers),
                          get_simple_features(velocity['touch_velocity'], "velocity"),
                          get_simple_features(
                              touch_acceleration(segment, velocity['touch_velocity'])['touch_acceleration'],
                              "acceleration")]

    return pd.concat(features_dataframe, axis=1)


# combined methods for getting features for sensors

def get_features_for_sensor(sensor, segment, columns=['x', 'y', 'z', 'm']):
    features_acc = []
    features_dataframe = []

    for column in columns:
        features_dataframe.append(get_simple_features(segment[column], column))
    features_acc.append(pd.concat(features_dataframe, axis=1))

    if not features_acc:
        return pd.DataFrame()

    return pd.concat(features_acc, axis=0)

	
	
	
def get_features_for_sensor_ACC(sensor, segment, columns=['x', 'y', 'z', 'm']):
    features_acc = []
    features_dataframe = []

   

    features_dataframe.append(get_simple_features_ACC_X(segment['x'], 'x'))
    features_dataframe.append(get_simple_features_ACC_Y(segment['y'], 'y'))
    features_dataframe.append(get_simple_features_ACC_Z(segment['z'], 'z'))
    features_dataframe.append(get_simple_features_ACC_M(segment['m'], 'm'))
    features_acc.append(pd.concat(features_dataframe, axis=1))

    if not features_acc:
        return pd.DataFrame()

    return pd.concat(features_acc, axis=0)
	
	
def get_features_for_sensor_GYRO(sensor, segment, columns=['x', 'y', 'z', 'm']):
    features_acc = []
    features_dataframe = []

   

    features_dataframe.append(get_simple_features_ACC_X(segment['x'], 'x'))
    features_dataframe.append(get_simple_features_ACC_Y(segment['y'], 'y'))
    features_dataframe.append(get_simple_features_ACC_Z(segment['z'], 'z'))
    features_dataframe.append(get_simple_features_ACC_M(segment['m'], 'm'))
    features_acc.append(pd.concat(features_dataframe, axis=1))

    if not features_acc:
        return pd.DataFrame()

    return pd.concat(features_acc, axis=0)	
	
	

def get_features_for_touch(segment, columns=['x', 'y', 'm']):
    features_touch = []
    features_dataframe = []

  
    features_dataframe.append(get_simple_features(segment['x'],'x'))
    features_dataframe.append(get_simple_features(segment[ 'y'],'y'))
    features_dataframe.append(get_simple_features(segment['m'],'m'))
	
      
    features_dataframe.append(get_touch_features_filtered(segment))
    features_touch.append(pd.concat(features_dataframe, axis=1))

    if not features_touch:
        return pd.DataFrame()

    return pd.concat(features_touch, axis=0)


# combined method for getting features for all sensors at once

def get_features_for_all_sensors(segments):
    all_features = {}

    for segment in segments:
        for key, value in segment.items():
            add_magnitude(value)
            final_key = key.split('_')[1]
            if final_key == 'move':
                features = get_features_for_touch(value)

            elif final_key == 'acc':
                  features = get_features_for_sensor_ACC(final_key, value)
            elif final_key == 'gyro':
                  features = get_features_for_sensor_GYRO(final_key, value)
            if not features.empty:
                if not final_key in all_features:
                    all_features[final_key] = []
                all_features[final_key].append(features)

    for key, value in all_features.items():
        all_features[key] = pd.concat(value, axis=0)
        #print(all_features[key].head())

    return all_features
