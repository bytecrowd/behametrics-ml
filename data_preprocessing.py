import numpy as np
import pandas as pd
import os


def save_dataframe(frame, frame_name, path):
    if not os.path.exists(path):
        os.makedirs(path)
    if not ".csv" in frame_name.lower():
        frame_name += ".csv"
    frame.to_csv(os.path.join(path, frame_name), sep=';', index=False)


def load_dataframe(frame_name, path):
    if not ".csv" in frame_name.lower():
        frame_name += ".csv"
    return pd.read_csv(os.path.join(path, frame_name), sep=";")


# save data from sensor (segment) to .csv

def split_by_sensor(segment, timestamp_key, sensors):
    splitted = {}

    for line in segment:
        # when running from flask, use this:
        if '\'' in str(line):
            line_splitted = line.split('\'')[1].split(';')
        # when not running from flask, use this:
        else:
            line_splitted = line.split(';')
        for sensor in sensors:
            if not sensor in line_splitted[1].lower():
                continue
            key = timestamp_key+'_'+sensor
            if not key in splitted:
                splitted[key] = []
            splitted[key].append(line_splitted)

    header_for_touch = ['IntType', 'StringType', 'Timestamp', 'Finger ID', 'Preasure', 'Size', 'x', 'y']
    header_for_other_sensors = ['IntType', 'StringType', 'Timestamp', 'x', 'y', 'z']
    for key, value in splitted.items():
        if 'move' in key:
            splitted[key] = pd.DataFrame(value, columns=header_for_touch)
            for column in ['Timestamp', 'Finger ID', 'Preasure', 'Size', 'x', 'y']:
                splitted[key][column] = splitted[key][column].astype(float)
        else:
            splitted[key] = pd.DataFrame(value, columns=header_for_other_sensors)
            for column in ['Timestamp', 'x', 'y', 'z']:
                splitted[key][column] = splitted[key][column].astype(float)
        splitted[key].set_index(['StringType'])
    return splitted


# split data into segments

def split_by_timestamp(root_directory, sensors=['acc', 'move', 'gyro'],
                       segment_period_ms=100000, sliding_window_ms=50000):
    segment = []
    start_timestamp = 0
    sliding_segment_index = 0
    final_segments = []
    i = 0
    for root, dirs, files in os.walk(root_directory):
        files.sort()
        for fi in files:
            if fi.split(".")[-1] == 'csv':
                frame = np.loadtxt(root + '/' + fi, delimiter='\n', dtype=str)
                for row in frame:
                    i += 1
                    if row.count(";") < 5:
                        print("error with file: " + fi + " row: " + row)
                        continue
                    row_sensor_name = row.split(';')[1].lower()
                    for sensor in sensors:
                        if not (sensor in row_sensor_name and not ("uncalib" in row_sensor_name)):
                            continue
                        # print(row_sensor_name)
                        timestamp = row.split(';')[2]
                        if int(start_timestamp) + int(segment_period_ms) - int(sliding_window_ms) > int(timestamp):
                            sliding_segment_index = len(segment)
                        if int(start_timestamp) == 0:
                            start_timestamp = timestamp
                            segment.append(row)
                        elif int(start_timestamp) + int(segment_period_ms) > int(timestamp):
                            segment.append(row)
                        else:
                            final_segments.append(split_by_sensor(segment, str(start_timestamp), sensors))
                            start_timestamp = int(timestamp) - int(sliding_window_ms)
                            segment = segment[sliding_segment_index:]
                            segment.append(row)
    # final segments is an array, one element is a dictionary with following structure:
    # 'timestamp_sensor': 'value'  ===> value is a dataframe containing raw_data from sensor
    #                                   during the period of one segment
    # example:
    #    '1515860335327_gyro':    IntType         StringType      Timestamp             x            y            z
    #                          0        4  K6DS3TR Gyroscope  1515860335384    -1.0677924   -0.2535824  0.042723913
    #                          1        4  K6DS3TR Gyroscope  1515860335404    -1.0152581  -0.26702142  0.112973414
    #    '1515860335327_acc' :    IntType         StringType      Timestamp             x            y            z
    #                          0    1  K6DS3TR Accelerometer  1515860335377    -1.7166426    4.8338933     7.823054
    #                          1    1  K6DS3TR Accelerometer  1515860335427   -0.24273849  -0.19994736  -0.46298552
    return final_segments


# load segments to frames and add headers

def load_data_from_sensors(folder, sensors=['move', 'acc', 'gyro']):
    folders = os.listdir(folder)
    header = []
    frames = []

    for file in folders:
        raw_sensors = os.listdir(folder + file)
        for sensor in sensors:
            raw_sensor = [s for s in raw_sensors if sensor.lower() in s.lower()]  # returns filename of chosen sensor
            if raw_sensor.__len__() == 0:
                continue
            path = os.path.join(folder, file, raw_sensor[0])
            print("loading: " + str(path))
            if not os.path.exists(path):
                continue
            if sensor.lower() in ['move', 'down', 'up']:
                header = ['IntType', 'StringType', 'Timestamp', 'Finger ID', 'Preasure', 'Size', 'x', 'y']
            else:
                header = ['IntType', 'StringType', 'Timestamp', 'x', 'y', 'z']
            frame = pd.read_csv(path, sep=';', names=header)
            frame.set_index(['StringType'])
            frames.append(frame)

    return frames


def get_negative_samples(path="/var/www/html/logger/datasets/default/SAMPLE_USER/segments/", number_of_samples=10,
                         sensors=['acc', 'gyro', 'touch']):
    dict = {}
    for sensor in sensors:
        if number_of_samples == 0:
            dict[sensor] = load_dataframe(sensor, path)
        else:
            dict[sensor] = load_dataframe(sensor, path).sample(n=number_of_samples)
        dict[sensor]['class'] = -1

    return dict
