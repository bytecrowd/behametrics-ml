#!/usr/bin/python3

from features import add_magnitude, get_features_for_all_sensors
from data_preprocessing import save_dataframe
from data_preprocessing import split_by_timestamp

import sys


def magnitude(segments):
    for segment in segments:
        add_magnitude(segment)


path = sys.argv[1]

sensors = [
    "MOVE",
    "Acc",
    "Gyro"
]

# print("loading data")
# segments = load_data_from_sensors(path + "split/", sensors)

print("spliting: " + str(path))
splitted_segments = split_by_timestamp(path, ['acc', 'move', 'gyro'])

print("counting features")
features = get_features_for_all_sensors(splitted_segments)

for key, value in features.items():
    print("saving " + str(key) + " features")
    save_dataframe(value, key, path + "segments/")
