#!/usr/bin/python3
import sys
import os
import pandas as pd
from data_preprocessing import load_dataframe, save_dataframe

path = sys.argv[1]

users = os.listdir(path)
sensors = ["acc.csv", "gyro.csv", "touch.csv"]
dict = {}

for sensor in sensors:
    dict[sensor] = []

print("getting random samples")
for user in users:
    folder_with_features = os.path.join(path, user, "segments/")
    for sensor in sensors:
        tmp = load_dataframe(sensor, folder_with_features)
        dict[sensor].append(tmp.sample(n=12))

sample_user = os.path.join(path, "SAMPLE_USER/segments/")
if not os.path.exists(sample_user):
    print("making dir: " + sample_user)
    os.makedirs(sample_user)

for key, value in dict.items():
    print("saving: " + sample_user + key)
    save_dataframe(pd.concat(value, axis=0, ignore_index=True), key, sample_user)
